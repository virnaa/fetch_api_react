import React, { Component } from "react";

class Turkey extends Component {

  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  componentDidMount() {
      fetch("https://randomuser.me/api/?results=10&nat=tr")
        .then(res => res.json())
        .then(parsedJSON => parsedJSON.results.map(data => (
          {
            id: `${data.id.name}`,
            firstName: `${data.name.first}`,
            lastName: `${data.name.last}`,
            location: `${data.location.state}, ${data.nat}`,gender: `${data.gender}`,
            location: `${data.location.street.number}, ${data.location.street.name},${data.location.city},${data.location.state}, ${data.location.country}`, 
            age: `${data.dob.age}`,
            phone: `${data.phone}`,
            thumbnail: `${data.picture.large}`,

          }
        )))
        .then(items => this.setState({
          items,
          isLoaded: false
        }))
        .catch(error => console.log('parsing failed', error))
    }

    render() {
      const {items } = this.state;
        return (
          <div className="boxWhite">
            <h2>Random User</h2>
            {
              items.length > 0 ? items.map(item => {
              const {id, firstName, lastName, location, age, phone, gender, thumbnail} = item;
               return (

               <div key={id} className="bgCircle">
               <center><img src={thumbnail} alt={firstName} className="circle"/> </center><br />
               <div className="ctr">
                  {firstName} {lastName}<br />
                  {gender}<br/>
                  {location}<br/>
                  {age}<br/>
                  {phone}<br/>

                </div>

              </div>
              );
            }) : null
          }
          </div>
        );

    }
  }

export default Turkey;